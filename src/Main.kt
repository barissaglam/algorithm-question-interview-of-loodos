import java.util.*
import java.util.logging.Handler

class Main {


    companion object {
        private val stringBuilder = StringBuilder()
        private val keyword = "abbcccaaeeeeb bfffffca ccab"
        @JvmStatic
        fun main(args: Array<String>) {
            setRepeatingCharacterAsStar(9,keyword)
            setRepeatingCharacterAsStar(5,keyword)
            setRepeatingCharacterAsStar(4,keyword)
            setRepeatingCharacterAsStar(3,keyword)
            setRepeatingCharacterAsStar(2,keyword)
        }

        private fun setRepeatingCharacterAsStar(repeatCount: Int, keyword: String) {
            if (keyword.isEmpty()) throw IllegalStateException("bos olamaz")
            this.stringBuilder.clear()
            var i = 0
            while (i < keyword.length) {
                val pair = compare(i, keyword)
                val repeat = pair.first
                val lastIndexOfRepeat = pair.second
                // Eğer hesaplanan harf tekrarı, belirlenen tekrar sayısına eşit ya bütyükse harfin ilk indexinden hesaplanan son indexe kadar döngü kurulup ilgili harfler * olarak atandı
                // içerdeki döngüde (i)'yi döngü sayısı kadar arttırma sebebimiz, o döngünün zaten ilgili harfleri * a çevirme amacıyla kurulması. * lanan harfleri ana döngüde tekrar kontrol etmeye gerek yok
                if (repeat >= repeatCount) {
                    for (j in i..lastIndexOfRepeat) {
                        stringBuilder.append("*")
                        i++
                    }
                } else {
                    stringBuilder.append(keyword[i])
                    i++
                }
            }

            System.out.println(stringBuilder.toString())
        }


        /**
         * Pair'in ilk elemanı gönderilen indexteki harfin kaç kere tekrar ettiğini temsil eder
         * Pair'in ikinci elemanı gönderilen indexteki harfin en son hangi indexe kadar tekrar ettiğini temsil eder
         */
        private fun compare(index: Int, keyword: String): Pair<Int, Int> {
            var count = 1
            for (j in index until keyword.length) {
                if ((j + 1) != keyword.length && keyword[j] == keyword[j + 1]) {
                    count++
                    continue
                } else {
                    return Pair(count, j)
                }
            }
            return Pair(0, 0)
        }
    }

}